# Ruby On Rails Refactor plugin for Sublime Text 3

This plugin adds the keybinding `CTRL` + `ALT` + `CMD` + `R` to refactor the selected code within a new method or service/interactor.

This is a _portage_ of my [ror-refactor](https://gitlab.com/zedtux/ror-refactor) plugin for [Atom](https://atom.io).
