import re
import sublime
import sublime_plugin

class RorRefactorCommand(sublime_plugin.TextCommand):
  #
  # 0. Plugin entry point
  #
  def run(self, edit):
    for region in self.view.sel():
      if not region.empty():
        # Get selected code
        selected = self.view.substr(region)
        # Determine refactoring method from selection
        refactor = getattr(self, self.refactoring_method_from(selected))
        # Exectues refactoring
        refactor(selected, region, edit)

  #
  # 1. Returns the refactoring method to be used
  #
  def refactoring_method_from(self, selected):
    # Method refactoring when the first line of `selected` has a `def ...`
    if re.match(r"^\s+def\s", selected, re.MULTILINE):
      return 'method'
    # Line refactoring when is a single line
    else:
      return 'line'

  #
  # 2. Single line refactoring method:
  #
  #   The selected line is moved within a new method
  #
  def line(self, selected, region, edit):
    # Get the current line number from where to move the code
    current_row = self.current_line_number()
    # Search for the place where to move the code (before the current method
    # definition, and comments if they are).
    # line is the position where we can write code
    # def_line is the line where is the method definition
    (line, def_line) = self.search_previous_def_from_current_line()

    # Exist if can't find where to move the code
    if (line == False):
      return False

    # In order to align the created method with the others, detect the
    # indentation and use it to write the new method
    indentation = self.detect_indentation_at_line(def_line)

    # Delete all cursors
    self.clear_region(edit, region)
    # Write new method and get a region of the new method definition line
    new_def_region = self.build_new_method_at(line, selected, indentation, edit)

    # Create new cursors
    self.add_selection_to(new_def_region)

  #
  # 2. Method refactoring method
  #
  #   The selected code, which includes at least one method, is moved within a
  #   new Interactor or Service.
  #
  def method(self, selected, region, edit):
    print("Methode refactoring")


  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # Helper tools to perform the refactoring
  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  #
  # Create a cursor at the given region
  #
  def add_selection_to(self, region):
    self.view.sel().add(region)

  #
  # Fetches the code at the given line number
  #
  def build_line_from(self, line_number):
    return self.view.line(self.move_region_to(line_number))

  #
  # Create the new method definition and add the selected code to its body with
  # an `end` with newlines on the next line.
  #
  def build_new_method_at(self, line, selected, indentation, edit):
    self.write_at_line(edit, ' ' * indentation + 'def \n', line)

    # Adds indentation is there isn't
    if indentation > 0 and re.search(r"^(\s+).*", selected) == None:
      selected = ' ' * (indentation + 2) + selected

    # Adds newline at selection if there isn't
    if (selected.endswith('\n') == False):
      selected += '\n'

    code = selected + ' ' * indentation + 'end\n\n'
    self.write_at_line(edit, code, line + 1)

    def_line_length = indentation + 4
    return sublime.Region(
      self.view.text_point(line, def_line_length),
      self.view.text_point(line, def_line_length)
    )

  #
  # Removes the copied code of the given region
  #
  def clear_region(self, edit, region):
    (row, col) = self.view.rowcol(region.a)
    indentation = self.detect_indentation_from(self.view.substr(region))

    # Removes last character from selected region if it is a newline so that
    # the erased code is not merging the next line within the current one
    if self.read_last_region_character(region) == '\n':
      region = sublime.Region(region.a, region.b - 1)

    self.view.erase(edit, region)
    self.view.insert(edit, region.begin(), ' ' * indentation)

    self.view.sel().clear()

    # Add a cursor to the right place
    self.add_selection_to(sublime.Region(
      region.begin() + indentation,
      region.begin() + indentation
    ))

  #
  # Returns the line number from the first selected line.
  #
  def current_line_number(self):
    (row, col) = self.view.rowcol(self.view.sel()[0].begin())
    return row

  #
  # Returns the indentation at the given line number
  #
  def detect_indentation_at_line(self, line_number):
    code = self.fetch_line_of_code_at(line_number)
    code = code.splitlines()[0]
    return self.detect_indentation_from(code)

  #
  # Returns the indentation from the given code
  #
  def detect_indentation_from(self, code):
    indentation = re.search(r"^(\s+).*", code)
    if indentation:
      detected_indentation = indentation.group(1)
      return len(detected_indentation)
    else:
      return 0

  #
  # Read code at the given line number
  #
  def fetch_line_of_code_at(self, line):
    previous_line = self.build_line_from(line)
    return self.view.substr(previous_line)

  #
  # Return True is the line is blank, a newline, or contains only spaces
  #
  def is_an_empty_line(self, code):
    if code == '' or code == "\n" or re.match(r"^\s+$", code):
      return True
    return False

  #
  # Build a new region for the given line number.
  #
  def move_region_to(self, line_number):
    return sublime.Region(
      self.view.text_point(line_number, 0),
      self.view.text_point(line_number + 1, 0)
    )

  #
  # Returns the last character from a given region
  #
  def read_last_region_character(self, region):
    last_region_character_region = sublime.Region(region.b - 1, region.b)
    return self.view.substr(last_region_character_region)

  #
  # Search for the previous method definition, also ignore comments, and return
  # the line number.
  #
  def search_previous_def_from_current_line(self):
    # Will be True as soon as a method definition has been found, see the Regex
    # bellow.
    method_definition_found = False

    method_definition_line_number = 0

    current_row = self.current_line_number()

    # Searching for the previous method definition
    while True:
      current_row -= 1
      previous_line_code = self.fetch_line_of_code_at(current_row)
      previous_line_code = previous_line_code.splitlines()[0]

      # First, find the previous method definition
      if method_definition_found == False:
        # Seach in the line of code method definition
        if re.match(r"^([\s]+)?def\s.*", previous_line_code):
          # Method definition found, now searching the first empty line
          method_definition_found = True
          method_definition_line_number = current_row
      else:
        # Now, find the first empty line or is full of whitetrailling spaces
        if self.is_an_empty_line(previous_line_code):
          return (current_row + 1, method_definition_line_number)

      # If no method definition and is at the first line, stop searching
      if current_row == 0:
        return (False, False)

  #
  # Write the code at the given line
  #
  def write_at_line(self, edit, code, line):
    write_at = self.build_line_from(line)
    self.view.insert(edit, write_at.begin(), code)
